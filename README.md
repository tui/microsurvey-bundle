# Tui MicroSurvey Bundle

This is a bundle for Symfony that provides an API for MicroSurvey User & Credentials retrieval.

## MicroSurvey API

TBD

## Installation - Symfony 4

Add the private repository to your `composer.json` file:

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:tui/microsurvey-bundle"
    }
]
```

And then `composer require tui/microsurvey-bundle`. As it's a private repository you should have an SSH key for your Bitbucket account.

Create the file `config/packages/tui_micro_survey.yaml` with the following content

```
tui_micro_survey:
    credentials_secret: '%env(TUI_MICROSURVEY_CREDENTIALS_SECRET)%'
    credentials_host: '%env(TUI_MICROSURVEY_CREDENTIALS_HOST)%'
```

For dev, just add any string as the value for the env file, but for UAT or Production find the secret value when either adding a site to the microsurvey service or look in the database (sites). The host should be the set to the microsurveys service production or UAT accordingly.

Finally, add a route for the MicroSurvey API in your `config/routes/annotations.yaml` file:

```
tui_micro_survey:
    resource: "@TuiMicroSurveyBundle/Controller/"
    type:     annotation
    prefix:   /api/microsurvey
```

## Installation - Symfony 3.4

Add the private repository to your `composer.json` file:

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:tui/microsurvey-bundle"
    }
]
```

And then `composer require tui/microsurvey-bundle`. As it's a private repository you should have an SSH key for your Bitbucket account.

Add the following to, `app/config/config.yaml`

```
tui_micro_survey:
    credentials_secret: '%env(TUI_MICROSURVEY_CREDENTIALS_SECRET)%'
    credentials_host: '%env(TUI_MICROSURVEY_CREDENTIALS_HOST)%'
```

Also add the following, ```new Tui\MicroSurveyBundle\TuiMicroSurveyBundle()```, to AppKernal.php

Finally, add a route for the MicroSurvey API in your `app/config/routing.yaml` file:

```
tui_micro_survey:
    resource: "@TuiMicroSurveyBundle/Controller/"
    type:     annotation
    prefix:   /api/microsurvey
```

Note: Getting the env variables to appear on the project changes a little per project, so I've left that part to the developer.
