<?php

namespace Tui\MicroSurveyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('tui_micro_survey');

        $rootNode
            ->children()
                ->scalarNode('credentials_secret')->isRequired()->end()
                ->scalarNode('credentials_host')->isRequired()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
