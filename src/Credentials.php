<?php

namespace Tui\MicroSurveyBundle;

/**
 * Retrieves the credentials needed to access the MicroSurvey service
 */
class Credentials
{
    /** @var string */
    protected $secret;

    /** @var string */
    protected $host;

    public function __construct(string $secret, string $host)
    {
        $this->secret = $secret;
        $this->host = $host;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function getHost(): string
    {
        return $this->host;
    }
}
