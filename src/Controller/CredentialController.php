<?php

namespace Tui\MicroSurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Tui\MicroSurveyBundle\Credentials;

/**
 * @Route("/credentials")
 */
class CredentialController extends AbstractController
{

    /** @var Credentials $credentials */
     protected $credentials;

     public function __construct(Credentials $credentials)
     {
         $this->credentials = $credentials;
     }

    /**
     *  Retrieve credentials, if they are available.
     *
     *  @Route("/", methods={"GET"})
     */
    public function retrieve(Request $request)
    {
        if (empty($this->credentials->getSecret())) {
            return new JsonResponse(null, 204);
        }

        return new JsonResponse([
            'secret' => $this->credentials->getSecret(),
            'host' => $this->credentials->getHost()
        ], 200);
    }
}
