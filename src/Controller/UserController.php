<?php

namespace Tui\MicroSurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/users")
 */
class UserController extends AbstractController
{
    /**
     *  Retrieve a user, if there is one available.
     *
     *  @Route("/", methods={"GET"})
     */
    public function retrieve(Request $request)
    {
        $storage = null;
        if ($this->container->has('security.token_storage')) {
            $storage = $this->container->get('security.token_storage');
        }

        if (isset($storage) && $storage->getToken() !== null) {
            $user = $storage->getToken()->getUser();
        } else {
            $user = $this->getUser();
        }

        if (empty($user)) {
            return new JsonResponse(null, 204);
        }

        return new JsonResponse(['username' => substr(sha1($user->getUsername()), 20)], 200);
    }
}
